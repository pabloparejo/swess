package com.lpsingenieria.swess;

/**
 * Created by parejo on 11/2/15.
 */
public class Consts {
    public static enum State{
        disabled, not_uploaded, to_review, valid, to_expire, expired, rejected
    }

    public final static String SERVER_URL = "http://www.lpsingenieria.es";
    public final static String FOLDER_PATH = "/swess/";
    public final static String WORKERS_URL = FOLDER_PATH + "qry_php/qry_worker.php";
    public final static String DOC_VIEWER_URL = "https://docs.google.com/gview?embedded=true&url=";
}

