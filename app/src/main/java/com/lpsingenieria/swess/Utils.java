package com.lpsingenieria.swess;

import java.util.ArrayList;

/**
 * Created by parejo on 18/2/15.
 */
public class Utils {

    public static String capitalize(String string){
        StringBuilder sb = new StringBuilder();
        String[] stringWords = string.split(" ");
        for (int i = 0; i < stringWords.length; i++) {
            sb.append(Character.toUpperCase(stringWords[i].charAt(0)) + stringWords[i].substring(1).toLowerCase() + " ");
        }
        
        return sb.toString();
    }
}
