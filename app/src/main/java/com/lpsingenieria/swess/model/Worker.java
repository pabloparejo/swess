package com.lpsingenieria.swess.model;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import com.lpsingenieria.swess.Consts;
import com.lpsingenieria.swess.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by parejo on 11/2/15.
 */
public class Worker implements Serializable{

    private final static String WORKER_DATA_URL = Consts.SERVER_URL + Consts.WORKERS_URL + "?do=worker_data&token=LPS_INGENIERIA&id_empleado=";

    private int mId;
    public String mName;
    public String mLastName;
    private String mCompany;
    public String mDni;
    private Consts.State mState;
    private Boolean mIsInTc;
    public String mPhone;
    public String mImage;
    public String mDocsUrl;
    public LinkedList<Document> mDocs;

    public Worker(int id, String name, String lastName, String company, String dni,
                  Consts.State state, Boolean isInTc, String phone, String image) {
        mId = id;
        mName = name;
        mLastName = lastName;
        mCompany = company;
        mDni = dni;
        mState = state;
        mIsInTc = isInTc;
        mPhone = phone;
        mImage = image;

        mDocsUrl = WORKER_DATA_URL + String.valueOf(id);
    }

    public String getImage() {
        return mImage;
    }

    @Override
    public String toString() {
        return Utils.capitalize(mName + " " + mLastName);
    }

    public int getId() {
        return mId;
    }

    public String getCompany() {
        return mCompany;
    }

    public String getState() {
        return mState.toString();
    }

    public Boolean getIsInTc() {
        return mIsInTc;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getDni() {
        return mDni;
    }

    public void setDni(String dni) {
        mDni = dni;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public List<Document> getDocs(){

        if (mDocs != null){
            return mDocs;
        }

        try{
            mDocs = new LinkedList<>();

            Log.v("SWESS", mDocsUrl);

            JSONObject jsonRoot = new JSONObject(JSONparser.getStringFromUrl(mDocsUrl));
            JSONArray docList = jsonRoot.optJSONArray("docs");
            for (int i = 0; i < docList.length(); i++) {
                mDocs.add(JSONparser.parseDoc(docList.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mDocs;
    }
}
