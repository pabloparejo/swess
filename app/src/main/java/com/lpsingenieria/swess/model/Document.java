package com.lpsingenieria.swess.model;

import com.lpsingenieria.swess.Consts;

import java.io.Serializable;

/**
 * Created by parejo on 18/2/15.
 */
public class Document implements Serializable {
    private int mId;
    private String mName;
    private Consts.State mState;
    private String mComment;
    private String mUrl;
    private String mUploadUrl;

    public Document(int id, String name, Consts.State state, String comment, String url, String uploadUrl) {
        mId = id;
        mName = name;
        mState = state;
        mComment = comment;
        mUrl = url;
        mUploadUrl = uploadUrl;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public Consts.State getState() {
        return mState;
    }

    public String getComment() {
        return mComment;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getUploadUrl() {
        return mUploadUrl;
    }

    public String toString(){
        return mName;
    }
}
