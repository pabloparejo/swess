package com.lpsingenieria.swess.model;

import android.util.Log;

import com.lpsingenieria.swess.Consts;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by parejo on 18/2/15.
 */
public class JSONparser {

    public static String getStringFromUrl(String url){
        StringBuilder sb = new StringBuilder();
        try{
            URLConnection conn = new URL(url).openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null){
                sb.append(line);
            }
            reader.close();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return sb.toString();

    }

    public static Worker parseWorker(JSONObject jsonWorker){
        Worker worker = new Worker(jsonWorker.optInt("id_empleado"),
                jsonWorker.optString("nombre_empleado"),
                jsonWorker.optString("apellidos_empleado"),
                jsonWorker.optString("empresa"),
                jsonWorker.optString("dni_empleado"),
                Consts.State.values()[jsonWorker.optInt("state")],
                jsonWorker.optBoolean("tc"),
                jsonWorker.optString("tlf_empleado"),
                Consts.SERVER_URL + "/swess/" + jsonWorker.optString("perfil_empleado")
        );
        return worker;
    }

    public static Document parseDoc(JSONObject jsonDoc){
        Document doc = new Document(jsonDoc.optInt("id_empleado"),
                jsonDoc.optString("nombre_documento"),
                Consts.State.values()[jsonDoc.optInt("state")],
                jsonDoc.optString("comentario"),
                Consts.DOC_VIEWER_URL + Consts.SERVER_URL + Consts.FOLDER_PATH + jsonDoc.optString("ruta_documento"),
                jsonDoc.optString("url_upload")
        );
        return doc;
    }
}
