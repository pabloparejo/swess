package com.lpsingenieria.swess.model;

import android.os.AsyncTask;
import android.util.Log;

import com.lpsingenieria.swess.Consts;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

/**
 * Created by parejo on 11/2/15.
 */

public class WorkersList {
    private static WorkersList mInstance = new WorkersList();
    private ArrayList<Worker> mWorkers;
    private LinkedList<WorkersListReady> mListeners = new LinkedList<WorkersListReady>();

    private static final String WORKERS_LIST_URL = Consts.SERVER_URL + "/swess/qry_php/qry_worker.php?do=workers_list&token=LPS_INGENIERIA";



    public static WorkersList getInstance() {
        return mInstance;
    }

    private WorkersList() {
        AsyncTask<Void, Void, ArrayList<Worker>> asyncTask = new AsyncTask<Void, Void, ArrayList<Worker>>() {
            @Override
            protected ArrayList<Worker> doInBackground(Void... params) {

                ArrayList<Worker> workers = new ArrayList<>();
                try{
                    Log.v("SWESS", WORKERS_LIST_URL);
                    URLConnection conn = new URL(WORKERS_LIST_URL).openConnection();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = null;
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null){
                        sb.append(line);
                    }
                    reader.close();

                    Log.v("JSON", sb.toString());

                    JSONArray jsonRoot = new JSONArray(sb.toString());
                    for (int i = 0; i < jsonRoot.length(); i++) {
                        workers.add(JSONparser.parseWorker(jsonRoot.getJSONObject(i)));
                    }
                } catch (Exception ex){
                    ex.printStackTrace();
                }
                return workers;
            }

            @Override
            protected void onPostExecute(ArrayList<Worker> workers) {
                super.onPostExecute(workers);
                mWorkers = workers;

                for (WorkersListReady listener : mListeners){
                    listener.onWorkersListReady(getClonedWorkersList());
                }
            }
        };
        asyncTask.execute();
    }

    public ArrayList<Worker> getClonedWorkersList(){
        ArrayList<Worker> copy = new ArrayList<Worker>();
        for (Worker worker : mWorkers) copy.add(worker);
        return copy;
    }

    public void addOnWorkersListReady(WorkersListReady listener){
        if (mWorkers == null){
            mListeners.add(listener);
        }else{
            listener.onWorkersListReady(mWorkers);
        }

    }

    public interface WorkersListReady{
        public void onWorkersListReady(ArrayList<Worker> workersList);
    }
}
