package com.lpsingenieria.swess.controller;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lpsingenieria.swess.R;
import com.lpsingenieria.swess.Utils;
import com.lpsingenieria.swess.model.Document;
import com.lpsingenieria.swess.model.Worker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

/**
 * Created by parejo on 17/2/15.
 */
public class WorkerFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String PREVIEW_URL = "https://docs.google.com/gview?embedded=true&url=";
    public final static String ARG_WORKER = "swess.controller.WorkerActivity.ARG_WORKER";

    private View mRoot = null;
    private ActionBar mActionBar = null;
    private OnDocumentClick mListener = null;

    private Worker mWorker= null;
    private ListView mDocsList = null;

    private DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder()
            .showImageOnLoading(null) // drawable de la imagen que se muestra durante la descarga
            .showImageForEmptyUri(null) // drawable de la imagen que se muestra si no tienes URI
            .showImageOnFail(null) // drawable de la imagen que se muestra si la descarga falla
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            //.displayer() // For circle imageView
            .build();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnDocumentClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDocumentClick");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        setHasOptionsMenu(true);
        mRoot = inflater.inflate(R.layout.fragment_worker, container, false);
        mWorker = (Worker) getArguments().getSerializable(ARG_WORKER);

        mActionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        mDocsList = (ListView) mRoot.findViewById(android.R.id.list);
        mDocsList.setOnItemClickListener(this);

        updateView();

        return mRoot;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void showWorker(Worker worker){
        mWorker = worker;
        updateView();
    }

    private void updateView(){
        final ProgressBar imageProgress = (ProgressBar) mRoot.findViewById(R.id.image_loading);
        imageProgress.setVisibility(View.VISIBLE);

        final ProgressBar docsProgress = (ProgressBar) mRoot.findViewById(R.id.docs_loading);
        docsProgress.setVisibility(View.VISIBLE);

        mActionBar.setTitle(mWorker.toString());


        final ImageView image = (ImageView) mRoot.findViewById(R.id.image);


        ImageLoader.getInstance().displayImage(mWorker.getImage(), image, mDisplayImageOptions, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imageProgress.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imageProgress.setVisibility(View.GONE);
                mActionBar.setIcon(new BitmapDrawable(getActivity().getResources(), loadedImage));
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                imageProgress.setVisibility(View.GONE);
            }
        });

        AsyncTask<Object, Integer, List<Document>> docsDownloader = new AsyncTask<Object, Integer, List<Document>>() {
            @Override
            protected List<Document> doInBackground(Object... params) {
                return mWorker.getDocs();
            }

            @Override
            protected void onPostExecute(List<Document> docs){
                ArrayAdapter<Document> adapter = new ArrayAdapter<Document>(getActivity(), android.R.layout.simple_list_item_1, mWorker.getDocs());
                mDocsList.setAdapter(adapter);
                docsProgress.setVisibility(View.GONE);
            }
        };

        docsDownloader.execute();

        TextView name = (TextView) mRoot.findViewById(R.id.name);
        name.setText(Utils.capitalize(mWorker.getName()));

        TextView lastName = (TextView) mRoot.findViewById(R.id.last_name);
        lastName.setText(Utils.capitalize(mWorker.getLastName()));

        TextView dni = (TextView) mRoot.findViewById(R.id.dni);
        dni.setText(mWorker.getDni());

        TextView phone = (TextView) mRoot.findViewById(R.id.phone);
        phone.setText(mWorker.getPhone());

        TextView company = (TextView) mRoot.findViewById(R.id.company);
        company.setText(Utils.capitalize(mWorker.getCompany()));

        TextView state = (TextView) mRoot.findViewById(R.id.state);
        state.setText(mWorker.getState());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            getActivity().finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.onDocumentClick(mWorker.getDocs().get(position));
    }

    public interface OnDocumentClick{
        void onDocumentClick(Document document);
    }
}
