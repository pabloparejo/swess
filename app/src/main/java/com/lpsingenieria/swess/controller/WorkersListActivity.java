package com.lpsingenieria.swess.controller;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.lpsingenieria.swess.Consts;
import com.lpsingenieria.swess.R;
import com.lpsingenieria.swess.model.Document;
import com.lpsingenieria.swess.model.Worker;
import com.lpsingenieria.swess.model.WorkersList;

import java.util.LinkedList;

public class WorkersListActivity extends ActionBarActivity implements AdapterView.OnItemClickListener, WorkersList.WorkersListReady, WorkerFragment.OnDocumentClick {

    private ArrayList<Worker> mWorkersList;
    private String INSTANCE_LIST_KEY = "com.lpsingenieria.swess.controller.WorkersListActivity.INSTANCE_LIST_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null && savedInstanceState.containsKey(INSTANCE_LIST_KEY)){
            mWorkersList = (ArrayList) savedInstanceState.getSerializable(INSTANCE_LIST_KEY);
        }

        if (mWorkersList == null){
            WorkersList.getInstance().addOnWorkersListReady(this);
        }

        setContentView(R.layout.activity_workers_list);
        FragmentManager fm = getFragmentManager();

        if (findViewById(R.id.list) != null){
            Fragment listFragment = fm.findFragmentById(R.id.list);
            if (listFragment == null){
                listFragment = new WorkersListFragment();
                fm.beginTransaction().add(R.id.list, listFragment).commit();
            }
        }

        if (findViewById(R.id.worker_frame) != null){
            // Estamos en horizontal, creamos worker fragment
            WorkerFragment workerFragment = (WorkerFragment) fm.findFragmentById(R.id.worker_frame);
            if (workerFragment == null){
                updateWorkerFragment(0);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (findViewById(R.id.worker_frame) != null){
            // Estamos en horizontal, actualizamos worker
            updateWorkerFragment(position);
        }else{
            Intent workerIntent = new Intent(this, WorkerActivity.class);
            workerIntent.putExtra(WorkerActivity.EXTRA_WORKER, mWorkersList.get(position));
            startActivity(workerIntent);
        }
    }

    @Override
    public void onWorkersListReady(ArrayList<Worker> workersList) {
        mWorkersList = workersList;
        if (findViewById(R.id.worker_frame) != null) {
            updateWorkerFragment(0);
        }
    }

    private void updateWorkerFragment(int position){
        FragmentManager fm = getFragmentManager();
        if (findViewById(R.id.worker_frame) != null && mWorkersList != null) {
            WorkerFragment workerFragment = (WorkerFragment) fm.findFragmentById(R.id.worker_frame);
            if (workerFragment == null) {
                Log.v("WORKER", "AQUISTAMOS");
                Bundle arguments = new Bundle();
                Worker worker = mWorkersList.get(position);
                arguments.putSerializable(WorkerFragment.ARG_WORKER, worker);
                workerFragment = new WorkerFragment();
                workerFragment.setArguments(arguments);
                fm.beginTransaction().add(R.id.worker_frame, workerFragment).commit();
            }else{
                workerFragment.showWorker(mWorkersList.get(position));
            }
        }
    }

    @Override
    public void onDocumentClick(Document document) {
        Intent docViewer = new Intent(this, WebActivity.class);
        docViewer.putExtra(WebActivity.EXTRA_URL, document.getUrl());
        docViewer.putExtra(WebActivity.EXTRA_TITLE, document.getName());
        startActivity(docViewer);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(INSTANCE_LIST_KEY, mWorkersList);
    }
}
