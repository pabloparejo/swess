package com.lpsingenieria.swess.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lpsingenieria.swess.R;
import com.lpsingenieria.swess.model.Worker;
import com.lpsingenieria.swess.model.WorkersList;

import java.util.ArrayList;
import java.util.List;


public class WorkersListFragment extends Fragment implements WorkersList.WorkersListReady {

    private AdapterView.OnItemClickListener mListener;
    private ArrayList mList;
    private ListView mListView;
    private ProgressDialog mProgressDialog;
    private String INSTANCE_LIST_KEY = "com.lpsingenieria.swess.controller.WorkersListFragment.INSTANCE_LIST_KEY";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_workers_list, container, false);
        mListView = (ListView) root.findViewById(android.R.id.list);

        if (savedInstanceState != null && savedInstanceState.containsKey(INSTANCE_LIST_KEY)){
            mList = (ArrayList) savedInstanceState.getSerializable(INSTANCE_LIST_KEY);
        }

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setTitle("Cargado trabajadores...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        if (mList == null){
            WorkersList.getInstance().addOnWorkersListReady(this);
        }else{
            displayList();
        }

        return root;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (AdapterView.OnItemClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onWorkersListReady(ArrayList<Worker> workersList) {
        mList = workersList;
        displayList();
    }

    private void displayList(){
        ArrayAdapter<Worker> adapter = new ArrayAdapter<Worker>(getActivity(), android.R.layout.simple_list_item_1, mList);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(mListener);
        mProgressDialog.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(INSTANCE_LIST_KEY, mList);
    }
}
