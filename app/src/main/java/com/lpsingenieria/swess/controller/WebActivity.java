package com.lpsingenieria.swess.controller;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by parejo on 21/3/15.
 */
public class WebActivity extends FragmentContainerActivity{

    public final static String EXTRA_URL = "swess.controller.WebActivity.EXTRA_URL";
    public final static String EXTRA_TITLE = "swess.controller.WebActivity.EXTRA_TITLE";

    @Override
    protected Fragment createFragment() {
        Bundle arguments = new Bundle();
        arguments.putSerializable(WebFragment.ARG_URL, getIntent().getSerializableExtra(EXTRA_URL));
        arguments.putSerializable(WebFragment.ARG_TITLE, getIntent().getSerializableExtra(EXTRA_TITLE));

        WebFragment fragment = new WebFragment();
        fragment.setArguments(arguments);

        return fragment;
    }
}
