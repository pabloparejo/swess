package com.lpsingenieria.swess.controller;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.lpsingenieria.swess.Consts;
import com.lpsingenieria.swess.R;
import com.lpsingenieria.swess.model.Document;
import com.lpsingenieria.swess.model.Worker;

public class WorkerActivity extends FragmentContainerActivity implements WorkerFragment.OnDocumentClick {
    public final static String EXTRA_WORKER = "swess.controller.WorkerActivity.EXTRA_WORKER";

    @Override
    protected Fragment createFragment() {
        Bundle arguments = new Bundle();
        arguments.putSerializable(WorkerFragment.ARG_WORKER, getIntent().getSerializableExtra(EXTRA_WORKER));
        WorkerFragment fragment = new WorkerFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onDocumentClick(Document document) {
        Intent docViewer = new Intent(this, WebActivity.class);
        docViewer.putExtra(WebActivity.EXTRA_URL, document.getUrl());
        docViewer.putExtra(WebActivity.EXTRA_TITLE, document.getName());
        startActivity(docViewer);
    }
}