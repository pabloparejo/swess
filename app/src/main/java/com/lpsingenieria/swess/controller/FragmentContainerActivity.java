package com.lpsingenieria.swess.controller;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.lpsingenieria.swess.R;

/**
 * Created by parejo on 11/2/15.
 */
public abstract class FragmentContainerActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);

        FragmentManager fm = getFragmentManager();

        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null){
            fragment = createFragment();
            fm  .beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit();
        }
    }

    protected abstract Fragment createFragment();
}