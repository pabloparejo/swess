package com.lpsingenieria.swess.controller;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.lpsingenieria.swess.R;
import com.lpsingenieria.swess.model.Worker;

public class WebFragment extends Fragment {
    public static final String ARG_URL = "swess.controller.WebFragment.ARG_URL";
    public static final String ARG_TITLE = "swess.controller.WebFragment.ARG_TITLE";


    private ActionBar mActionBar = null;
    private View mRoot = null;
    private String mURL;
    private WebView mBrowser = null;
    private ProgressBar mProgess = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragment_web, container, false);

        mURL = getArguments().getString(ARG_URL);
        mActionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setTitle(getArguments().getString(ARG_TITLE, "Web"));
        setHasOptionsMenu(true);

        mBrowser = (WebView) mRoot.findViewById(R.id.webView);
        mProgess = (ProgressBar) mRoot.findViewById(R.id.progressBar);

        mBrowser.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgess.setVisibility(View.VISIBLE);
            }



            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgess.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                mProgess.setVisibility(View.GONE);
            }
        });

        mBrowser.getSettings().setBuiltInZoomControls(true);
        mBrowser.getSettings().setJavaScriptEnabled(true);


        mBrowser.loadUrl(mURL);

        return mRoot;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
